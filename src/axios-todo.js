import axios from 'axios';

const instance = axios.create({
  baseURL: 'https://homework-alexandrcher.firebaseio.com/'
});

export default instance;