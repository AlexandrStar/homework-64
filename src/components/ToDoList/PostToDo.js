import React from 'react';
import './FormToDo.css';

const PostToDo = (props) =>  {

        return (
            props.entries.map((task) => {
                return (
                    <div className='task-todo' key={task.id}>
                        <p>{task.text}</p>
                        <button onClick={() => props.remove(task.id)} className='btn-remove-todo'>Remove</button>
                    </div>
                )
            })
        )

};

export default PostToDo;