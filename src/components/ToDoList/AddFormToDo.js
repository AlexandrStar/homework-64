import React from 'react';
import './FormToDo.css';

const AddFormToDo = (props) => {
        return (
            <div>
            <div className='task-form-todo'>
                <input
                  onChange={props.change}
                  className='task-input-todo'
                  type="text"
                  placeholder="Add new task" />
                <button
                  onClick={props.add}
                  className='btn-add-todo'
                >Add</button>
            </div>
            </div>
        )
};


export default AddFormToDo;