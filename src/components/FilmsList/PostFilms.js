import React, {Component} from 'react';
import './FormFilms.css';


class PostFilms extends Component {

  shouldComponentUpdate(nextProps) {
    return nextProps.value !== this.props.value;
  }

  render() {
    return (
      <div
        className='task-film'
        key={this.props.id}>
        <input
          className="filmName"
          value={this.props.value}
          onChange={this.props.onChange}
          />
        <button
          onClick={this.props.remove}
          className='btn-remove-film'
        >Remove</button>
      </div>
    )
  }

}

export default PostFilms;