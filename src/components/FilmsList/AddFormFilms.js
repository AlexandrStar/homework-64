import React from 'react';
import './FormFilms.css';

const AddFormFilms = (props) => {
    return (
        <div>
        <div className='task-form-film'>
            <input
              onChange={props.change}
              className='task-input-film'
              type="text"
            />
            <button
              type="reset"
              onClick={props.add}
              className='btn-add-film'
            >Add</button>
        </div>
        </div>
    )
};


export default AddFormFilms;