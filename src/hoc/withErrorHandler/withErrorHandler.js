import React, {Component, Fragment} from 'react';
import Spinner from "../../components/UI/Spinner/Spinner";

const withErrorHandler = (WrappedComponent, axios) => {
  return class WithErrorHOC extends Component {

    constructor(props) {
      super(props);

      this.state = {
        error: null,
        loading: false,
      };

      axios.interceptors.request.use(req => {
        console.log('request');
        this.setState({loading: true});
        return req;
      });

      axios.interceptors.response.use(res => {
        this.setState({loading: false});
        return res;
      }, error => {
        this.setState({error});
        throw error;
      });
    }

    render() {
      return (
        <Fragment>
          <WrappedComponent {...this.props} />
          {this.state.loading? <Spinner /> : null}
        </Fragment>
      );
    };
  };
};

export default withErrorHandler;