import React, {Component, Fragment} from 'react';

import {Collapse, Container, Nav, Navbar, NavbarBrand, NavbarToggler, NavItem, NavLink} from "reactstrap";
import {Route, Switch, NavLink as RouterNavLink} from "react-router-dom";
import FilmsList from "./containers/FilmsList";

import './App.css';
import ToDoList from "./containers/ToDoList";

class App extends Component {
  render() {
    return (
      <Fragment>
        <Navbar color="light" light expand="md">
          <NavbarBrand>ToDo Central</NavbarBrand>
          <NavbarToggler />
          <Collapse isOpen navbar>
            <Nav className="ml-auto" navbar>
              <NavItem>
                <NavLink tag={RouterNavLink} to="/" exact >List</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={RouterNavLink} to="/todo/list">ToDo list</NavLink>
              </NavItem>
              <NavItem>
                <NavLink tag={RouterNavLink} to="/film/list">Film list</NavLink>
              </NavItem>
            </Nav>
          </Collapse>
        </Navbar>

        <Container>
          <Switch>
            <Route path="/" exact render={() => <h1>Home</h1>} />
            <Route path="/todo/list" exact component={ToDoList} />
            <Route path="/film/list" exact component={FilmsList} />
            <Route render={() => <h1>Not found</h1>} />
          </Switch>
        </Container>
      </Fragment>
    );
  }
}

export default App;
