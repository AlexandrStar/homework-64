import React, { Component } from 'react';

import AddFormToDo from '../components/ToDoList/AddFormToDo';
import PostToDo from "../components/ToDoList/PostToDo";
import axios from '../axios-todo';

import './ToDoList.css';
import withErrorHandler from "../hoc/withErrorHandler/withErrorHandler";

class ToDoList extends Component {

    state = {
        tasks: [],
        textInput: '',
    };

    getPosts = () => {
      let url = 'posts.json';
      axios.get(url).then(response => {
        if (!response.data) return this.setState({tasks: []});
        const tasks = Object.keys(response.data).map(id => {
          return {...response.data[id], id}
        });
        this.setState({tasks});
      })
    };

  componentDidMount() {
    this.getPosts()
  }


    addTask = () => {
        if (this.state.textInput !== '') {
            const newTask = {
                text: this.state.textInput
            };
          axios.post('posts.json', newTask).then(() => this.getPosts());
        }
    };

    removeItems = id => {
        const tasks = [...this.state.tasks];
        tasks.splice(id, 1);
      axios.delete('posts/' + id + '.json').then(() => this.getPosts())
    };

    changeHandler = (event) => {
        this.setState({
            textInput: event.target.value
        })
    };

    render() {
        return (
            <div className="ToDoList">
                <AddFormToDo change={(event) => this.changeHandler(event)} add={() => this.addTask()}/>
                <PostToDo entries = {this.state.tasks}
                          remove = {(id) => this.removeItems(id)}
                />
            </div>
        );
    }
}

export default withErrorHandler(ToDoList, axios);
