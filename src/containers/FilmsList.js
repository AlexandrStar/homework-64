import React, { Component } from 'react';

import AddFormFilms from '../components/FilmsList/AddFormFilms';
import PostFilms from "../components/FilmsList/PostFilms";

import './FilmList.css';
import axios from "../axios-todo";
import withErrorHandler from "../hoc/withErrorHandler/withErrorHandler";

class FilmsList extends Component {

    state = {
        tasks: [],
        textInput: '',
    };

    getFilms = () => {
      let url = 'films.json';
      axios.get(url).then(response => {
        if (!response.data) return this.setState({tasks: []});
        const tasks = Object.keys(response.data).map(id => {
          return {...response.data[id], id}
        });
        this.setState({tasks});
      })
    };

    componentDidMount() {
      this.getFilms()
    }

    addTaskFilms = () => {
        if (this.state.textInput !== '') {
            const newTaskFilms = {
                text: this.state.textInput
            };
          axios.post('films.json', newTaskFilms).then(() => this.getFilms());
        }
    };

    removeItems = id => {
        const tasksFilms = this.state.tasks;
        tasksFilms.splice(id, 1);
      axios.delete('films/' + id + '.json').then(() => this.getFilms())
    };

    changeHandler = (event) => {
        this.setState({
            textInput: event.target.value
        })
    };

    changeFilmName = (event, index) => {
      let tasks = this.state.tasks;
      tasks[index].text = event.target.value;
      this.setState({tasks});
    };

    render() {
        return (
            <div className="FilmsList">
                <AddFormFilms
                     change={(event) => this.changeHandler(event)}
                     add={() => this.addTaskFilms()}
                />
              <div className='tasks-film'>
              <p>To watch list:</p>
              {this.state.tasks.map((task, id) => (
                <PostFilms
                  key={id}
                  value={task.text}
                  remove = {() => this.removeItems(task.id)}
                  onChange={task => this.changeFilmName(task, id)}
                />
              ))}
            </div>
            </div>
        );
    }
}

export default withErrorHandler(FilmsList, axios);
